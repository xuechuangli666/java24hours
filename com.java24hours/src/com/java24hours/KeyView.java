package com.java24hours;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyView extends JFrame implements KeyListener {
    JTextField keyText = new JTextField(80);
    JLabel keyLabel = new JLabel("Press and key in the text field.");

    public KeyView() {
        super("KeyViewer");
        setLookAndFeel();
        setSize(360, 100);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        keyText.addKeyListener(this);
        BorderLayout bord = new BorderLayout();
        setLayout(bord);
        add(keyLabel, BorderLayout.NORTH);
        add(keyText, BorderLayout.CENTER);
        setVisible(true);
    }

    public void keyTyped(KeyEvent input) {
        char key = input.getKeyChar();
        keyLabel.setText("You pressed " + key);
    }

    public void keyPressed(KeyEvent txt) {
        // do nothing
    }

    public void keyReleased(KeyEvent txt) {
        // do nothing
    }

    private void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(
                    "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
            );
        } catch (Exception exc) {
            // ignore error
        }
    }

    public static void main(String[] args) {
        KeyView frame = new KeyView();
    }
}
